/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678


*/

const init = () => {
    signInBtn = document.getElementById('sign-in')
    exitBtn = document.getElementById('exit')

    const email = 'admin@gmail.com'
    const pass = '12345678'

    let elements = Array.from(document.forms.myForm.elements)

    signInBtn.disabled = true
    if (localStorageIsEmpty()) {
        hideForm('hide')
        displayBtnExit('display')
        userHi('hello')
    } else {
        elements.forEach(function(element){
            element.addEventListener('input', function(e) {
                if (!element.validity.valid) {
                    element.style.border = '1px solid red'
                    signInBtn.disabled = true
                } else {
                    element.style.border = '1px solid green'
                    signInBtn.disabled = false
                }
            })
            console.dir(document.forms.myForm)
        })
        signInBtn.addEventListener('click', function(btn){
            btn.preventDefault()
            if (document.getElementById('email').value === email && document.getElementById('pass').value === pass) {
                saveToLocalStorage()
                hideForm('hide')
                displayBtnExit('display')
                userHi('hello')
            }
        })
    }

    exitBtn.addEventListener('click', function(e){
        displayBtnExit('hide')
        hideForm('display')
        userHi('bye')
        localStorage.setItem('login', '')
        localStorage.setItem('password', '')

    })
    function displayBtnExit (val) {
        if (val === 'display') {
            document.getElementById('exit').style.display = 'block'
        } else if (val === 'hide') {
            document.getElementById('exit').style.display = 'none'
        }
    }
    function hideForm(val) {
        if (val === 'hide') {
            document.forms.myForm.style.display = 'none'
        } else if (val === 'display') {
            document.forms.myForm.style.display = 'block'
        }
    }
    function saveToLocalStorage() {
        localStorage.setItem('login', email)
        localStorage.setItem('password', pass)
    }
    function userHi(val) {
        let res = document.getElementById('hiUser')
        if (val === 'hello') {
            res.innerHTML = `Hellow ${email}`
        } else if (val === 'bye') {
            res.innerHTML = null
        }
    } 
    function localStorageIsEmpty () {
        if (localStorage.getItem('login') && localStorage.getItem('password') !== null) {
            return true
        } else {
            return false
        }
    }
}
document.addEventListener('DOMContentLoaded', init)