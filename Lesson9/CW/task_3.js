/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/
const init = () => {

    class Post {
        constructor(id, isActive, title, about, likes, created_at) {
            this.id = id
            this.isActive = isActive
            this.title = title
            this.about = about
            this.likes = likes
            this.created_at = created_at
        }

        addLike () {
            this.likes += 1
        }
    }

    let arrayPosts = []

    let btnSendPost = document.getElementById('send-post')

    btnSendPost.addEventListener('click', sendPost)

    let id = 0
    function sendPost(e) {
        e.preventDefault()
        let title = document.getElementById('title').value
        let about = document.getElementById('about').value
        let likes = 0
        let created = new Date()
        let post = new Post(id, true, title, about, likes, created)
        console.log(post)
        arrayPosts.push(post)
        console.log(arrayPosts)

        let res = document.getElementById('res-post')
        let myPost = `
        <h3>Title</h3>
        <div>${post.title}</div>
        <h2>Post</h2>
        <div>${post.about}</div>
        <h5>Created at: ${post.created_at}</h5>
        <button class="btn-likes" id="${post.id}">Likes ${post.likes}</button>
        <hr>
        `
        setTimeout(function(){
            let btnLikes = document.querySelectorAll('.btn-likes')
            btnLikes.forEach(function(btn){
                btn.addEventListener('click', Like)
            })
        }, 1000)
        let div = document.createElement('div')
        div.innerHTML = myPost
        res.appendChild(div)
        localStorage.setItem('post' + id, JSON.stringify(post))
        id++
    }

    function Like(e){
        console.log(e.target)
        console.log(arrayPosts[e.target.id])
        arrayPosts[e.target.id].addLike()
        e.target.innerHTML = `Likes ${arrayPosts[e.target.id].likes}`
    }

    if (localStorage.getItem("post0") !== null){
        for (let i = 0; i < localStorage.length; i++) {
            console.log(JSON.parse(localStorage.getItem("post"+i)))
            let post = new Post(JSON.parse(localStorage.getItem("post"+i)).id, true, JSON.parse(localStorage.getItem("post"+i)).title, JSON.parse(localStorage.getItem("post"+i)).about, JSON.parse(localStorage.getItem("post"+i)).likes, JSON.parse(localStorage.getItem("post"+i)).created)

            let res = document.getElementById('res-post')
            let myPost = `
            <h3>Title</h3>
            <div>${post.title}</div>
            <h2>Post</h2>
            <div>${post.about}</div>
            <h5>Created at: ${post.created_at}</h5>
            <button class="btn-likes" id="${post.id}">Likes ${post.likes}</button>
            <hr>
            `
            setTimeout(function(){
                let btnLikes = document.querySelectorAll('.btn-likes')
                btnLikes.forEach(function(btn){
                    btn.addEventListener('click', Like)
                })
            }, 1000)

            let div = document.createElement('div')
            div.innerHTML = myPost
            res.appendChild(div)
        }
    }

}

document.addEventListener('DOMContentLoaded', init)