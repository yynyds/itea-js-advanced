/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/

const init = () => {

    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
      }
      
      var bgColor
      
      function generateRandomColor(){
        bgColor = `rgb(${getRandomIntInclusive(0, 255)}, ${getRandomIntInclusive(0, 255)}, ${getRandomIntInclusive(0, 255)})`;
      }

    let btn = document.getElementById('random-color')
    if (localStorage.getItem("color") !== null) {
        btn.addEventListener('click', function() {
            document.body.style.backgroundColor = localStorage.getItem("color");
        })
    } else {
        generateRandomColor();
        localStorage.setItem('color', bgColor);
        btn.addEventListener('click', function() {
        document.body.style.backgroundColor = localStorage.getItem("color");
    })
    }
}
document.addEventListener('DOMContentLoaded', init)