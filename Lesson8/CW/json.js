
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <input />

  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/

const init = () => {
  let formGenerateJson = document.forms.generateJson;
  let name = formGenerateJson.name
  let age = formGenerateJson.age
  let password = formGenerateJson.password
  let btn = document.getElementById('generateJson')
  let btnParse = document.getElementById('parse')
  let jsonObj = {}

  console.dir(name, age, password, btn);

  function generateJson(e) {
    e.preventDefault()
    jsonObj[name.name] = name.value
    jsonObj[age.name] = age.value
    jsonObj[password.name] = password.value

    let myJson = document.forms.parseJson.json
    myJson.value = JSON.stringify(jsonObj)

    console.log('jsonObj:', jsonObj)
  }

  function parseJson(e) {
    e.preventDefault()
    let myJson = document.forms.parseJson.json
    let res = JSON.parse(myJson.value)
    console.log(res)
  }

  btn.addEventListener('click', generateJson)
  btnParse.addEventListener('click', parseJson)

}
document.addEventListener('DOMContentLoaded', init)