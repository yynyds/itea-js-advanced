/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

const init = () => {

  let resFormedObj = document.getElementById('resFormedObj')

  var myHeaders = new Headers();
      myHeaders.append("Content-Type", "text/plain");
      myHeaders.append("Content-Length", 255);
      myHeaders.append("X-Custom-Header", "ProcessThisImmediately");

  let url = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2'
  let options = {
    method: 'GET',
    header: myHeaders
  }

  function status(res) {
    if (res.status !== 200) {
      return new Promise.reject(new Error(res.statusText))
    }
    return Promise.resolve(res)
  }
  
  function json(res) {
    return res.json()
  }

  fetch(url, options)
  .then(status)
  .then(json)
  .then(data => {
    let randomIndex = Math.random() * (9 - 0) + 0
    randomIndex = Math.floor(randomIndex)
    userData = data[randomIndex]
    return data[randomIndex]
  })
  .then(user => {
    return fetch(url, options)
    .then(status)
    .then(json)
    .then(fiends => {
      console.log('user', user)
      console.log('frineds', fiends)
      let myObj = {
        name: user.name,
        age: user.age,
        about: user.about,
        address: user.address,
        company: user.company,
        friends: fiends
      }
      return myObj
    })
    .catch(error => {
      console.log('error', error)
    })
  })
  .then(formedObj => {
    console.log('formedObj', formedObj)
    let code = document.createElement('pre')
    str = JSON.stringify(formedObj)
    str = str.replace(/","/gi, `${'",'}\n${'"'}`)
    code.innerHTML = str
    resFormedObj.appendChild(code)
  })
  .catch(error => {
    console.log('error', error)
  })



}
document.addEventListener('DOMContentLoaded', init)


  // fetch(url)
  //   .then(testFunc)
  //   .then(test2Func)
  //   .then( res => {
  //      return fetch()
  //       .then( friendsResponse)
  //       .then()
  //   })
  //   .then( func)
