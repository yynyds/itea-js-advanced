/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
const init = () => {

  let companyData = [];

  async function getData () {
    const data = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
    companyData = await data.json();
    await drawTable(companyData);
    await setBtnRegistrationEvent();
    await setBtnAdrEvent();
    console.log('companyData', companyData);
  }

  function drawTable (data) {
    let table = `<table>
                  <thead>
                    <tr>
                      <td>№</td>
                      <td>Company name</td>
                      <td>Balance</td>
                      <td>Ragistration date</td>
                      <td>Check address</td>
                    </tr>
                  </thead>
                  <tbody>
                  ${
                    data.map((item, index) => {
                      return (`<tr>
                                <td>${index + 1}</td>
                                <td>${item.company}</td>
                                <td>${item.balance}</td>
                                <td class="btns-reg">
                                  <button data-id="${item._id}">View rigistration date</button>
                                </td>
                                <td id="${item._id}" class="btns-adr">
                                  <button data-id="${item._id}">View address</button>
                                </td>
                              </tr>`)
                    }).join('')
                  }
                  </tbody>
                </table>`;
    let res = document.getElementById('resTable');
    res.innerHTML = table
  }

  function setBtnRegistrationEvent() {
    let btnFullAddress = document.querySelectorAll('.btns-reg > button');
    console.log(btnFullAddress);
    btnFullAddress.forEach(btn => {
      btn.addEventListener('click', displayRegistration);
    })
  }

  function setBtnAdrEvent() {
    let btnFullAddress = document.querySelectorAll('.btns-adr > button');
    console.log(btnFullAddress);
    btnFullAddress.forEach(btn => {
      btn.addEventListener('click', displayAddress);
    })
  }

  function displayRegistration (e) {
    let findedObh = companyData.find(item => item._id === e.target.attributes[0].nodeValue);
    alert(findedObh.registered)
  }

  function displayAddress (e) {
    let findedObh = companyData.find(item => item._id === e.target.attributes[0].nodeValue);
    console.log(findedObh);
    console.log(joinAddress(findedObh.address));
    alert(joinAddress(findedObh.address));
  }

  function joinAddress (address) {
    return address.zip + ' ' + address.country + ' ' + address.city + ' ' + address.state + ' ' + address.street + ' ' + address.house;
  }
  
  getData() 
}

document.addEventListener('DOMContentLoaded', init)