
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
 function getCountBtn () {
  var wrapBtn = document.getElementById('buttonContainer');
  return wrapBtn.childElementCount;
 }

 function setClass (val) {
  var tab = document.querySelector('#tabContainer > .tab[data-tab="'+val+'"]')
  tab.classList.add('active')
 }

 function hideTab (val) {
  var tab = document.querySelector('#tabContainer > .tab[data-tab="'+val+'"]')
  tab.classList.remove('active')
 }

 var btns = []
 for (let i = 0; i < getCountBtn(); i++) {
   let num = i + 1
   btns[i] = document.querySelector('#buttonContainer .showButton[data-tab="'+num+'"]');
   btns[i].onclick = function() {
     setClass(num)
     for (let i = 1; i < 4; i++) {
       if (i !== num) {
        hideTab(i)
       }
     }
     console.dir(btns[i])
   }
 }