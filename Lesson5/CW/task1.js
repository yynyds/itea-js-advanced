/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/


function Train(name, speed, countPassenger) {
    this.name = name;
    this.speed = speed;
    this.countPassenger = countPassenger;
    this.drive = function() {
        console.log('Поезд ' + this.name + ' везет ' + this.countPassenger + ' со скоростью ' + this.speed)
    }
    this.stop = function() {
        console.log('Поезд ' + this.name + ' остановился. Скорость ' + this.speed)
    }
    this.getPessenger = function(x) {
        this.countPassenger += x
        console.log('Увеличивает кол-во пассажиров на ' + x)
    }
}

var myTrain = new Train('Yamaha', 120, 5)

myTrain.drive()
myTrain.stop()
myTrain.getPessenger(4)
myTrain.drive()

