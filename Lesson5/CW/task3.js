/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства


    Dog {
      name: '',
      breed: '',
      status: 'idle',

      changeStatus: function(){...},
      showProps: function(){...}
    }

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/

function Dog(name, breed) {
  this.name = name;
  this.breed = breed;
} 

function otherRunDog(prop) {
  console.log(this.name + ' bejit s beshinoi skorostyiu. Ego status: ' + this[prop])
}

function otherStopDog(prop, prop2) {
  this[prop] = false
  console.log(this.name + ' ostanovylsia. Ego status: ' + this[prop])
  if (prop2) {
    console.log('On ' + this[prop2]) 
  }
}

function displayPropsList() {
  for(key in this) {
    console.log(key, this[key])
  }
}

function displayPropsListV2 (obj) {
  for(key in obj) {
    console.log(key, obj[key])
  }
}

let myDog = new Dog('Pes', 'Akita-inu')
myDog.status = true
myDog.eat = 'kushaet'
myDog.runDog = function () {
  console.log(this.name + ' bejit')
}
myDog.stopDog = function () {
  console.log(this.name + ' stoyt')
}
myDog.runDog()
myDog.stopDog()
otherRunDog.call(myDog, 'status')
let arrayProps = ['status', 'eat'] // ['status']
otherStopDog.apply(myDog, arrayProps)
console.log(myDog)
displayPropsList.call(myDog)
displayPropsListV2(myDog)