/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/
CommentsArray = []

const CommentProto = {
  linkToAvatar: 'defaulLink',
  likes: 0,
  addCountLikes: function () {
    this.likes += 1
  }
}

function Comment(name, textSms, linkToAvatar) {
  this.name = name;
  this.textSms = textSms;
  this.likes = 0;
  if (linkToAvatar !== undefined) {
    this.linkToAvatar = linkToAvatar;
  }
  Object.setPrototypeOf(this, CommentProto)
  CommentsArray.push(this)
}





let myCommet1 = new Comment('post1', 'Simple sms1', 'img1')
let myCommet2 = new Comment('post2', 'Simple sms1', 'img2')
let myCommet3 = new Comment('post3', 'Simple sms1', 'img3')
console.log('CommentsArray', CommentsArray)

let CommentsFeed = document.getElementById('CommentsFeed')


CommentsArray.forEach(element => {
  let title = document.createElement('h2')
  let likes = document.createElement('span')
  let sms = document.createElement('div')
  let img = document.createElement('div')
  title.textContent = element.name
  likes.textContent = element.likes
  sms.textContent = element.textSms
  img.textContent = element.linkToAvatar
  CommentsFeed.appendChild(title)
  CommentsFeed.appendChild(likes)
  CommentsFeed.appendChild(sms)
  CommentsFeed.appendChild(img)
  title.addEventListener('click', function(e){
    likes.textContent = ''
    element.addCountLikes()
    likes.textContent += element.likes
  })
});





